/*  	AutoLevellerAE (http://www.autoleveller.co.uk) is a stand-alone PC application written in Java which is designed
 *  	to measure precisely the height of the material to be milled / etched in several places,
 *  	then use the information gathered to make adjustments to the Z height
 *  	during the milling / etching process so that a more consistent and accurate result can be achieved.
 *
 *   	Copyright (C) 2013 James Hawthorne PhD, daedelus1982@gmail.com
 *
 *   	This program is free software; you can redistribute it and/or modify
 *   	it under the terms of the GNU General Public License as published by
 *   	the Free Software Foundation; either version 2 of the License, or
 *   	(at your option) any later version.
 *
 *   	This program is distributed in the hope that it will be useful,
 *   	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   	GNU General Public License for more details.
 *
 *   	You should have received a copy of the GNU General Public License along
 *   	with this program; if not, see http://www.gnu.org/licenses/
*/
package com.cncsoftwaretools.autoleveller;

import org.apache.commons.lang3.builder.EqualsBuilder;

/**
 * Created by James Hawthorne on 23/10/2017.
 */
public class UpdateVer implements Version
{
    private final MajorMinor majorMinor;
    private final int upNum;

    public final static Version IDENTITY = newInstance("", 0, 0.0, 0);

    private UpdateVer(String name, int majorNum, Double minorNum, int upNum)
    {
        this.majorMinor = MajorMinor.newInstance(name, majorNum, minorNum);
        this.upNum = upNum;
    }

    public static UpdateVer newInstance(String name, int majorNum, Double minorNum, int upNum)
    {
        return new UpdateVer(name, majorNum, minorNum, upNum);
    }

    @Override
    public int getMajorNum()
    {
        return majorMinor.getMajorNum();
    }

    @Override
    public double getMinorNum()
    {
        return majorMinor.getMinorNum();
    }

    public int getUpdateNum(){return upNum;}

    @Override
    public int compareVersions(Version otherVersion)
    {
        UpdateVer otherUpdateVer = (UpdateVer)otherVersion;
        int majorMinorEq = Version.super.compareVersions(otherVersion);

        return majorMinorEq == 0 ? new Integer(upNum).compareTo(otherUpdateVer.getUpdateNum()) : majorMinorEq;
    }

    @Override
    public String toString()
    {
        return majorMinor.toString() + "u" + upNum;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        UpdateVer updateVer = (UpdateVer) o;

        return new org.apache.commons.lang3.builder.EqualsBuilder()
                .append(upNum, updateVer.upNum)
                .append(majorMinor, updateVer.majorMinor)
                .isEquals();
    }

    @Override
    public int hashCode()
    {
        return new org.apache.commons.lang3.builder.HashCodeBuilder(17, 37)
                .append(majorMinor)
                .append(upNum)
                .toHashCode();
    }
}
