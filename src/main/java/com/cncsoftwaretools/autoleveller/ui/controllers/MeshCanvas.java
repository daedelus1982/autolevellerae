/*  	AutoLevellerAE (http://www.autoleveller.co.uk) is a stand-alone PC application written in Java which is designed
 *  	to measure precisely the height of the material to be milled / etched in several places,
 *  	then use the information gathered to make adjustments to the Z height
 *  	during the milling / etching process so that a more consistent and accurate result can be achieved.
 *
 *   	Copyright (C) 2013 James Hawthorne PhD, daedelus1982@gmail.com
 *
 *   	This program is free software; you can redistribute it and/or modify
 *   	it under the terms of the GNU General Public License as published by
 *   	the Free Software Foundation; either version 2 of the License, or
 *   	(at your option) any later version.
 *
 *   	This program is distributed in the hope that it will be useful,
 *   	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   	GNU General Public License for more details.
 *
 *   	You should have received a copy of the GNU General Public License along
 *   	with this program; if not, see http://www.gnu.org/licenses/
*/
package com.cncsoftwaretools.autoleveller.ui.controllers;

import com.cncsoftwaretools.autoleveller.Drawing;
import com.cncsoftwaretools.autoleveller.Mesh;
import com.cncsoftwaretools.autoleveller.TriConsumer;
import com.cncsoftwaretools.autoleveller.util.GraphicsContextWrapper;
import com.google.common.collect.ImmutableList;
import javafx.geometry.Point2D;
import javafx.scene.canvas.Canvas;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

import java.util.List;

/**
 * Created by James Hawthorne on 11/06/2017.
 *
 */
public class MeshCanvas implements Drawing
{
    private final Rectangle originalSize;
    private final Rectangle currentSize;
    private final List<TriConsumer<GraphicsContextWrapper, Double, Point2D>> drawingInstructions;

    MeshCanvas(Mesh mesh, Canvas canvas)
    {
        originalSize = new Rectangle(mesh.getSize().getX(), mesh.getSize().getY(), mesh.getSize().getWidth(), mesh.getSize().getHeight());
        currentSize = mesh.getSize();
        ImmutableList.Builder<TriConsumer<GraphicsContextWrapper, Double, Point2D>> instructionBuilder = ImmutableList.builder();
        instructionBuilder.add((gc, ratio, offset) -> {
            gc.setStroke(Color.RED);
            gc.setFill(Color.BROWN);
            double tlPointX = offset.getX() * ratio;
            double tlPointY = canvas.getHeight() - (currentSize.getHeight() + offset.getY()) * ratio;
            Rectangle drawnRect = new Rectangle(tlPointX, tlPointY, currentSize.getWidth() * ratio, currentSize.getHeight() * ratio);
            gc.strokeRect(drawnRect.getX(), drawnRect.getY(), drawnRect.getWidth(), drawnRect.getHeight());

            mesh.getPointsAsList().forEach(point -> {
                double radius = 2;
                double x = drawnRect.getX() + ((point.get2DPoint().getX() - currentSize.getX()) * ratio) - radius;
                double y = drawnRect.getY() + ((point.get2DPoint().getY() - currentSize.getY()) * ratio) - radius;
                gc.fillOval(x, y, radius * 2, radius * 2);
            });
        });

        drawingInstructions = instructionBuilder.build();
    }

    @Override
    public Rectangle originalSize()
    {
        return originalSize;
    }

    @Override
    public List<TriConsumer<GraphicsContextWrapper, Double, Point2D>> getDrawingInstructions()
    {
        return drawingInstructions;
    }
}
