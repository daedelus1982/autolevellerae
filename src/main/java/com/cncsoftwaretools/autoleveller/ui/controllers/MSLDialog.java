/*  	AutoLevellerAE (http://www.autoleveller.co.uk) is a stand-alone PC application written in Java which is designed
 *  	to measure precisely the height of the material to be milled / etched in several places,
 *  	then use the information gathered to make adjustments to the Z height
 *  	during the milling / etching process so that a more consistent and accurate result can be achieved.
 *
 *   	Copyright (C) 2013 James Hawthorne PhD, daedelus1982@gmail.com
 *
 *   	This program is free software; you can redistribute it and/or modify
 *   	it under the terms of the GNU General Public License as published by
 *   	the Free Software Foundation; either version 2 of the License, or
 *   	(at your option) any later version.
 *
 *   	This program is distributed in the hope that it will be useful,
 *   	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   	GNU General Public License for more details.
 *
 *   	You should have received a copy of the GNU General Public License along
 *   	with this program; if not, see http://www.gnu.org/licenses/
*/
package com.cncsoftwaretools.autoleveller.ui.controllers;

import com.cncsoftwaretools.autoleveller.Units;
import com.cncsoftwaretools.autoleveller.util.StringDouble3DPConverter;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

/**
 * Created by James Hawthorne on 28/03/2017.
 *
 */
public class MSLDialog extends Stage implements Initializable
{
    @FXML private Spinner<Double> mslSpn;

    private Double mslValue;

    private MSLDialog()
    {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/ALGUI/MaxSegLgt.fxml"));
        fxmlLoader.setController(this);

        try {
            setScene(new Scene(fxmlLoader.load()));
            Stage stage = (Stage) getScene().getWindow();
            stage.getIcons().add(new Image("alicon.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static MSLDialog newInstance()
    {
        return new MSLDialog();
    }

    Optional<Double> popupAndWait(Double currentMSL, Units currentUnits)
    {
        mslValue = null;
        setFactory(currentUnits, currentMSL);
        showAndWait();

        return Optional.ofNullable(mslValue);
    }

    private void setFactory(Units currentUnits, Double initVal)
    {
        SpinnerValueFactory<Double> segLthSpnFac = currentUnits.equals(Units.MM) ?
                new SpinnerValueFactory.DoubleSpinnerValueFactory(1.0, 300.0, initVal, 1.0) :
                new SpinnerValueFactory.DoubleSpinnerValueFactory(0.01, 10.0, initVal, 0.005);

        segLthSpnFac.setConverter(new StringDouble3DPConverter());
        mslSpn.setValueFactory(segLthSpnFac);
    }

    @FXML
    private void onCancel()
    {
        mslValue = null;
        close();
    }

    @FXML
    private void onOK()
    {
        mslValue = mslSpn.getValue();
        close();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        setResizable(false);
        setTitle("Maximum Segment Length");
    }
}
