/*  	AutoLevellerAE (http://www.autoleveller.co.uk) is a stand-alone PC application written in Java which is designed
 *  	to measure precisely the height of the material to be milled / etched in several places,
 *  	then use the information gathered to make adjustments to the Z height
 *  	during the milling / etching process so that a more consistent and accurate result can be achieved.
 *
 *   	Copyright (C) 2013 James Hawthorne PhD, daedelus1982@gmail.com
 *
 *   	This program is free software; you can redistribute it and/or modify
 *   	it under the terms of the GNU General Public License as published by
 *   	the Free Software Foundation; either version 2 of the License, or
 *   	(at your option) any later version.
 *
 *   	This program is distributed in the hope that it will be useful,
 *   	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   	GNU General Public License for more details.
 *
 *   	You should have received a copy of the GNU General Public License along
 *   	with this program; if not, see http://www.gnu.org/licenses/
*/
package com.cncsoftwaretools.autoleveller.ui.drawables;

import com.google.common.collect.ImmutableList;
import com.google.inject.BindingAnnotation;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import javafx.geometry.BoundingBox;
import javafx.scene.PerspectiveCamera;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Transform;
import javafx.scene.transform.Translate;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.util.List;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 *
 * Created by James Hawthorne on 15/09/2016.
 */
public class FocusedTransformableCamera implements TransformableCamera
{
    private final BoundingBox focusArea;

    public final PerspectiveCamera theCamera;
    public final double theCameraDistance;
    public final double theFarClip;
    public final Translate centerTranslation;
    public final Rotate zRotate;
    public final Rotate xRotate;
    public final Translate camDistance;

    @Inject
    public FocusedTransformableCamera(@Assisted BoundingBox focusArea, @ZRotate Rotate zRotate,
                                      @XRotate Rotate xRotate, Translate camDistance, PerspectiveCamera camera)
    {
        this.theCamera = camera;
        this.focusArea = focusArea;
        this.centerTranslation = getCenterTranslation();
        this.theCameraDistance = getCameraDistance();
        this.theFarClip = this.theCameraDistance * 4;
        this.zRotate = zRotate;
        this.xRotate = xRotate;
        camDistance.setZ(this.theCameraDistance);
        this.camDistance = camDistance;

        setOrderedTransforms(ImmutableList.of(this.centerTranslation, this.zRotate, this.xRotate, this.camDistance));
        this.theCamera.setNearClip(0.1);
        this.theCamera.setFarClip(this.theFarClip);
    }

    private Translate getCenterTranslation()
    {
        Translate centerTranslation = new Translate();

        centerTranslation.setX(focusArea.getMinX()+(focusArea.getWidth()/2));
        centerTranslation.setY(-(focusArea.getMinY()+(focusArea.getHeight()/2)));
        centerTranslation.setZ(focusArea.getMinZ()+(focusArea.getDepth()/2));

        return centerTranslation;
    }

    private double getCameraDistance()
    {
        final double fov = Math.toRadians(theCamera.getFieldOfView());

        //a^2 + b^2 = c^2
        final double cornerToCorner = Math.sqrt(Math.pow(focusArea.getWidth(), 2) +
                Math.pow(focusArea.getHeight(), 2));

        return -((cornerToCorner/2) / (Math.tan(fov)/2));
    }

    @Override
    public void setOrderedTransforms(List<Transform> transforms)
    {
        theCamera.getTransforms().clear();
        theCamera.getTransforms().addAll(transforms);
    }

    @BindingAnnotation
    @Target({ FIELD, PARAMETER, METHOD }) @Retention(RUNTIME)
    public @interface ZRotate {}

    @BindingAnnotation
    @Target({ FIELD, PARAMETER, METHOD }) @Retention(RUNTIME)
    public @interface XRotate {}
}
