/*  	AutoLevellerAE (http://www.autoleveller.co.uk) is a stand-alone PC application written in Java which is designed
 *  	to measure precisely the height of the material to be milled / etched in several places,
 *  	then use the information gathered to make adjustments to the Z height
 *  	during the milling / etching process so that a more consistent and accurate result can be achieved.
 *
 *   	Copyright (C) 2013 James Hawthorne PhD, daedelus1982@gmail.com
 *
 *   	This program is free software; you can redistribute it and/or modify
 *   	it under the terms of the GNU General Public License as published by
 *   	the Free Software Foundation; either version 2 of the License, or
 *   	(at your option) any later version.
 *
 *   	This program is distributed in the hope that it will be useful,
 *   	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   	GNU General Public License for more details.
 *
 *   	You should have received a copy of the GNU General Public License along
 *   	with this program; if not, see http://www.gnu.org/licenses/
*/
package com.cncsoftwaretools.autoleveller.ui.drawables;

import com.cncsoftwaretools.autoleveller.Units;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.paint.Material;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Box;
import javafx.scene.shape.Shape3D;
import javafx.scene.shape.Sphere;

import java.util.function.DoubleUnaryOperator;

public class Axis3dModelGroup
{
    private final static double axisLengthInMM = 50;
    private final static double axisBreadthInMM = 0.25;

    private final DoubleUnaryOperator convertMMValueToCurrentUnits;
    public final Group theAxisGroup;

    @Inject
    private Axis3dModelGroup(@Assisted Units units, Group axisGroup)
    {
        this.convertMMValueToCurrentUnits = mmValue -> units.equals(Units.MM) ? mmValue : Units.mmToInches(mmValue);
        theAxisGroup = setAxisModel(axisGroup);
    }

    private Group setAxisModel(Group axisGroup)
    {
        final Shape3D zeroPoint = createScaledZeroMarker();
        final Shape3D xAxis = createScaledAxisFor(Axis3D.XAXIS);
        final Shape3D yAxis = createScaledAxisFor(Axis3D.YAXIS);
        final Shape3D zAxis = createScaledAxisFor(Axis3D.ZAXIS);
        final Shape3D xAxisPositiveEndStop = createScaledEndStopFor(Axis3D.XAXIS);
        final Shape3D yAxisPositiveEndStop = createScaledEndStopFor(Axis3D.YAXIS);
        final Shape3D zAxisPositiveEndStop = createScaledEndStopFor(Axis3D.ZAXIS);

        axisGroup.getChildren().addAll(zeroPoint, xAxis, yAxis, zAxis);
        axisGroup.getChildren().addAll(xAxisPositiveEndStop, yAxisPositiveEndStop, zAxisPositiveEndStop);

        return axisGroup;
    }

    private Shape3D createScaledZeroMarker()
    {
        final double zeroMarkerRadiusInMM = convertMMValueToCurrentUnits.applyAsDouble(0.5);
        final PhongMaterial goldMaterial = new PhongMaterial(Color.GOLD);
        final Sphere zeroPoint = new Sphere(zeroMarkerRadiusInMM);

        zeroPoint.setMaterial(goldMaterial);

        return zeroPoint;
    }

    private Shape3D createScaledAxisFor(Axis3D axis3D)
    {
        final double width = convertMMValueToCurrentUnits.applyAsDouble(axis3D.widthInMM);
        final double height = convertMMValueToCurrentUnits.applyAsDouble(axis3D.heightInMM);
        final double depth = convertMMValueToCurrentUnits.applyAsDouble(axis3D.depthInMM);

        final Shape3D axis = new Box(width, height, depth);
        axis.setMaterial(axis3D.diffuseMaterial);

        return axis;
    }

    private Shape3D createScaledEndStopFor(Axis3D axis3D)
    {
        final double axisEndStopRadius = convertMMValueToCurrentUnits.applyAsDouble(0.5);
        final double halfAxisLength = convertMMValueToCurrentUnits.applyAsDouble(axisLengthInMM) / 2;
        final Sphere axisPositiveEndStop = new Sphere(axisEndStopRadius);

        axisPositiveEndStop.setMaterial(axis3D.diffuseMaterial);
        if (axis3D.equals(Axis3D.XAXIS)){axisPositiveEndStop.setTranslateX(halfAxisLength);}
        if (axis3D.equals(Axis3D.YAXIS)){axisPositiveEndStop.setTranslateY(-halfAxisLength);}
        if (axis3D.equals(Axis3D.ZAXIS)){axisPositiveEndStop.setTranslateZ(-halfAxisLength);}

        return axisPositiveEndStop;
    }

    private enum Axis3D
    {
        XAXIS(new PhongMaterial(Color.RED), axisLengthInMM, axisBreadthInMM, axisBreadthInMM),
        YAXIS(new PhongMaterial(Color.GREEN), axisBreadthInMM, axisLengthInMM, axisBreadthInMM),
        ZAXIS(new PhongMaterial(Color.BLUE), axisBreadthInMM, axisBreadthInMM, axisLengthInMM);

        final Material diffuseMaterial;
        final double widthInMM;
        final double heightInMM;
        final double depthInMM;

        Axis3D(Material diffuseMaterial, double widthInMM, double heightInMM, double depthInMM)
        {
            this.diffuseMaterial = diffuseMaterial;
            this.widthInMM = widthInMM;
            this.heightInMM = heightInMM;
            this.depthInMM = depthInMM;
        }
    }

}
