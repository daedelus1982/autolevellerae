/*  	AutoLevellerAE (http://www.autoleveller.co.uk) is a stand-alone PC application written in Java which is designed
 *  	to measure precisely the height of the material to be milled / etched in several places,
 *  	then use the information gathered to make adjustments to the Z height
 *  	during the milling / etching process so that a more consistent and accurate result can be achieved.
 *
 *   	Copyright (C) 2013 James Hawthorne PhD, daedelus1982@gmail.com
 *
 *   	This program is free software; you can redistribute it and/or modify
 *   	it under the terms of the GNU General Public License as published by
 *   	the Free Software Foundation; either version 2 of the License, or
 *   	(at your option) any later version.
 *
 *   	This program is distributed in the hope that it will be useful,
 *   	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   	GNU General Public License for more details.
 *
 *   	You should have received a copy of the GNU General Public License along
 *   	with this program; if not, see http://www.gnu.org/licenses/
*/
package com.cncsoftwaretools.autoleveller.ui;

import com.cncsoftwaretools.autoleveller.ui.drawables.FocusedTransformableCamera;
import com.cncsoftwaretools.autoleveller.ui.drawables.TransformableCamera;
import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import javafx.scene.SubScene;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Transform;
import javafx.scene.transform.Translate;

import java.util.List;

/**
 *
 * Created by James Hawthorne on 23/09/2016.
 */
public class MouseEventHandler implements MouseEventSetter
{
    private double xScenePoint = 0;
    private double yScenePoint = 0;
    private double zAngle = 0;
    private double xAngle = 0;

    private final double camDistance;
    private final double farClip;
    private final Rotate camZRotate;
    private final Rotate camXRotate;
    private final Translate camZTranslate;
    private final Translate centerTranslate;
    private final TransformableCamera camera;

    @Inject
    public MouseEventHandler(@Assisted FocusedTransformableCamera camera)
    {
        this.camDistance = camera.theCameraDistance;
        this.camZTranslate = camera.camDistance;
        this.farClip = camera.theFarClip;
        this.centerTranslate = camera.centerTranslation;
        this.camXRotate = camera.xRotate;
        this.camZRotate = camera.zRotate;
        this.camera = camera;
        initializeTransforms();
    }

    private void initializeTransforms()
    {
        camZTranslate.setZ(camDistance);
        List<Transform> camTransforms = ImmutableList.of(centerTranslate, camZRotate,
                camXRotate, camZTranslate);

        camera.setOrderedTransforms(camTransforms);
    }

    @Override
    public void setMouseEvents(SubScene subScene)
    {
        subScene.setOnMousePressed(this::onClickEvent);
        subScene.setOnMouseDragged(this::onDragEvent);
        subScene.setOnScroll(this::onScrollEvent);
    }

    private void onClickEvent(MouseEvent me)
    {
        xScenePoint = me.getSceneX();
        yScenePoint = me.getSceneY();
        zAngle = camZRotate.getAngle();
        xAngle = camXRotate.getAngle();

        me.consume();
    }

    private void onDragEvent(MouseEvent me)
    {
        camZRotate.setAngle(zAngle - (xScenePoint - me.getSceneX()));
        camXRotate.setAngle(xAngle + yScenePoint - me.getSceneY());

        me.consume();
    }

    private void onScrollEvent(ScrollEvent scrollEvent)
    {
        final double scrollablesegments = 40;
        final double scrollUnit = -(scrollEvent.getDeltaY() / Math.abs(scrollEvent.getDeltaY()));
        final double zoomDelta = scrollUnit * (camDistance/scrollablesegments);
        double zTranslate = Math.min(0, Math.max((camZTranslate.getZ() + zoomDelta), farClip));
        camZTranslate.setZ(zTranslate);

        scrollEvent.consume();
    }
}
