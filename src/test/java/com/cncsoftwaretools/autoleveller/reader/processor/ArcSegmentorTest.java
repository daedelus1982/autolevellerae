package com.cncsoftwaretools.autoleveller.reader.processor;

import com.cncsoftwaretools.autoleveller.reader.GCodeState;
import com.cncsoftwaretools.autoleveller.util.Arc;
import javafx.geometry.Point2D;
import org.junit.Test;

import java.util.List;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;

public class ArcSegmentorTest
{
    private ArcSegmentProcessor segmentor = ArcSegmentProcessor.newInstance(Optional.empty(), 25);

    @Test
    public void nextState_noPreviousState()
    {
        GCodeState state = GCodeState.newInstance(Optional.<GCodeState>empty(), "G3 X5.3755 Y3.1824 I0.0 J-1.1093");

        Optional<GCodeState> nextState = segmentor.nextState(state);

        //no previous state, cant get X or Y
        assertThat(nextState, equalTo(Optional.empty()));
    }

    @Test
    public void nextState_firstG2Segment()
    {
        GCodeState firstState = GCodeState.newInstance(Optional.<GCodeState>empty(), "G1 X15.91549 Y0 Z-1 F300");
        GCodeState blankState = GCodeState.newInstance(Optional.of(firstState), "");
        GCodeState arcState = GCodeState.newInstance(Optional.of(blankState), "G2 X15.91549 Y0 I-15.91549");

        Optional<GCodeState> nextState = segmentor.nextState(arcState);

        assertThat(nextState.get(), equalTo(GCodeState.newInstance(Optional.of(blankState),
                "G2 X-0.000007 Y-15.91549 I-15.91549 J0 ;segmented arc. Max segment length set to 25")));
    }

    @Test
    public void arcAsGCodeLine()
    {
        Arc arc = Arc.newInstance(new Point2D(15.91549, 0), new Point2D(0, -15.91549), Point2D.ZERO, Arc.Direction.CW);

        String gCodeLine = segmentor.arcAsGCodeString(arc, GCodeState.newInstance(Optional.empty(), ""));

        assertThat(gCodeLine, equalTo("G2 X0 Y-15.91549 I-15.91549 J0"));
    }

    @Test
    public void segmentedArcFromState_centreFormat()
    {
        GCodeState firstState = GCodeState.newInstance(Optional.<GCodeState>empty(), "G1 X15.91549 Y0 Z-1 F300");
        GCodeState blankState = GCodeState.newInstance(Optional.of(firstState), "");
        GCodeState arcState = GCodeState.newInstance(Optional.of(blankState), "G2 X15.91549 Y0 I-15.91549");

        Arc arc = segmentor.segmentedArcFromState(arcState).get();

        assertThat(arc, equalTo(Arc.newInstance(
                new Point2D(15.91549, 0), new Point2D(-7.0E-6, -15.91549), Point2D.ZERO, Arc.Direction.CW)));
    }

    @Test
    public void nextState_feedrateMissing()
    {
        GCodeState firstState = GCodeState.newInstance(Optional.<GCodeState>empty(), "G1 X15.91549 Y0 Z-1");
        GCodeState blankState = GCodeState.newInstance(Optional.of(firstState), "");
        GCodeState arcState = GCodeState.newInstance(Optional.of(blankState), "G2 X15.91549 Y0 I-15.91549");

        Optional<GCodeState> nextState = segmentor.nextState(arcState);

        assertThat(nextState, equalTo(Optional.empty()));
    }

    @Test
    public void nextState_startPointIncomplete()
    {
        GCodeState firstState = GCodeState.newInstance(Optional.<GCodeState>empty(), "G1 X15.91549 Z-1 F300");
        GCodeState blankState = GCodeState.newInstance(Optional.of(firstState), "");
        GCodeState arcState = GCodeState.newInstance(Optional.of(blankState), "G2 X15.91549 Y0 I-15.91549");

        Optional<GCodeState> nextState = segmentor.nextState(arcState);

        assertThat(nextState, equalTo(Optional.empty()));
    }

    @Test
    public void nextState_IandJMissing()
    {
        GCodeState firstState = GCodeState.newInstance(Optional.<GCodeState>empty(), "G1 X15.91549 Y0 Z-1 F800");
        GCodeState blankState = GCodeState.newInstance(Optional.of(firstState), "");
        GCodeState arcState = GCodeState.newInstance(Optional.of(blankState), "G2 X15.91549 Y0");

        Optional<GCodeState> nextState = segmentor.nextState(arcState);

        assertThat(nextState, equalTo(Optional.empty()));
    }

    @Test
    public void nextState_zTooHigh()
    {
        GCodeState firstState = GCodeState.newInstance(Optional.<GCodeState>empty(), "G1 X15.91549Y0 Z1 F300");
        GCodeState blankState = GCodeState.newInstance(Optional.of(firstState), "");
        GCodeState arcState = GCodeState.newInstance(Optional.of(blankState), "G2 X15.91549 Y0 I-15.91549");

        Optional<GCodeState> nextState = segmentor.nextState(arcState);

        assertThat(nextState, equalTo(Optional.empty()));
    }

    @Test
    public void nextState_CWRadius()
    {
        ArcSegmentProcessor segmentorLength10 = ArcSegmentProcessor.newInstance(Optional.empty(), 10);

        GCodeState firstState = GCodeState.newInstance(Optional.<GCodeState>empty(), "G1 X-4.184334 Y-3.596896 Z-2 F3000");
        GCodeState blankState = GCodeState.newInstance(Optional.of(firstState), "");
        GCodeState arcState = GCodeState.newInstance(Optional.of(blankState), "G02 X18.5 Y46 R29.338370 F300");

        Optional<GCodeState> nextState = segmentorLength10.nextState(arcState);

        assertThat(nextState, equalTo(Optional.of(GCodeState.newInstance(Optional.of(blankState),
                "G2 X-9.75064 Y4.652457 I21.184332 J20.296897 F300 " +
                ";segmented arc. Max segment length set to 10"))));
    }

    @Test
    public void absoluteIJKMode_shouldTreatIJASAbsoluteCentreXY()
    {
        ArcSegmentProcessor segmentorLength10 = ArcSegmentProcessor.newInstance(Optional.empty(), 10);

        GCodeState zeroState = GCodeState.newInstance(Optional.empty(), "N545 G90.1");
        GCodeState firstState = GCodeState.newInstance(Optional.of(zeroState), "N550 G1 X-7 Y33 Z-2 F3000");
        GCodeState blankState = GCodeState.newInstance(Optional.of(firstState), "");
        GCodeState arcState = GCodeState.newInstance(Optional.of(blankState), "N555 G2 X42.9 Y32 I17 J16.7 F300");

        Optional<GCodeState> nextState = segmentorLength10.nextState(arcState);

        assertThat(nextState, equalTo(Optional.of(GCodeState.newInstance(Optional.of(blankState),
                "G2 X-0.080564 Y40.150892 I17 J16.7 F300 ;segmented arc. Max segment length set to 10"))));
    }

    @Test
    public void relativeIJKMode_shouldTreatIJrelativeToStartPoint()
    {
        ArcSegmentProcessor segmentorLength10 = ArcSegmentProcessor.newInstance(Optional.empty(), 10);

        GCodeState zeroState = GCodeState.newInstance(Optional.empty(), "N545 G91.1");
        GCodeState firstState = GCodeState.newInstance(Optional.of(zeroState), "G1 X-4.184334 Y-3.596896 Z-2 F3000");
        GCodeState blankState = GCodeState.newInstance(Optional.of(firstState), "");
        GCodeState arcState = GCodeState.newInstance(Optional.of(blankState), "G02 X18.5 Y46 I21.184332 J20.296897 F300");
                Optional<GCodeState> nextState = segmentorLength10.nextState(arcState);

        assertThat(nextState, equalTo(Optional.of(GCodeState.newInstance(Optional.of(blankState),
                "G2 X-9.75064 Y4.652457 I21.184332 J20.296897 F300 " +
                        ";segmented arc. Max segment length set to 10"))));
    }

    @Test
    public void nextState_CCWRadius()
    {
        ArcSegmentProcessor segmentorLength10 = ArcSegmentProcessor.newInstance(Optional.empty(), 10);

        GCodeState firstState = GCodeState.newInstance(Optional.<GCodeState>empty(), "G1 X18.5 Y46 Z-0.1 F3000");
        GCodeState blankState = GCodeState.newInstance(Optional.of(firstState), "");
        GCodeState arcState = GCodeState.newInstance(Optional.of(blankState), "G03 X-4.184334 Y-3.596896 R29.338370 F300");

        Optional<GCodeState> nextState = segmentorLength10.nextState(arcState);

        assertThat(nextState, equalTo(Optional.of(GCodeState.newInstance(Optional.of(blankState),
                "G3 X8.619043 Y44.815825 I-1.500002 J-29.299999 F300 " +
                        ";segmented arc. Max segment length set to 10"))));
    }

    @Test
    public void processState_arcCCWRadius()
    {
        ArcSegmentProcessor segmentorLength10 = ArcSegmentProcessor.newInstance(Optional.empty(), 10);

        GCodeState firstState = GCodeState.newInstance(Optional.<GCodeState>empty(), "G1 X18.5 Y46 Z-1 F3000");
        GCodeState blankState = GCodeState.newInstance(Optional.of(firstState), "");
        GCodeState arcState = GCodeState.newInstance(Optional.of(blankState), "G03 X44.5 Y6 R-29.5 F300");

        segmentorLength10.processState(firstState);
        segmentorLength10.processState(blankState);
        List<GCodeState> segmentedStates = segmentorLength10.processState(arcState);

        GCodeState previousState = blankState;

        assertThat(segmentedStates.get(0), equalTo(previousState = GCodeState.newInstance(Optional.of(previousState),
                "G3 X8.615665 Y44.839827 I-1.552493 J-29.45912 F300 ;segmented arc. Max segment length set to 10")));
        assertThat(segmentedStates.get(1), equalTo(previousState = GCodeState.newInstance(Optional.of(previousState),
                "G3 X-0.320395 Y40.458855 I8.331842 J-28.298947 F300 ;segmented arc. Max segment length set to 10")));
        assertThat(segmentedStates.get(2), equalTo(previousState = GCodeState.newInstance(Optional.of(previousState),
                "G3 X-7.291137 Y33.355698 I17.267902 J-23.917975 F300 ;segmented arc. Max segment length set to 10")));
        assertThat(segmentedStates.get(3), equalTo(previousState = GCodeState.newInstance(Optional.of(previousState),
                "G3 X-11.503197 Y24.33879 I24.238644 J-16.814818 F300 ;segmented arc. Max segment length set to 10")));
        assertThat(segmentedStates.get(4), equalTo(previousState = GCodeState.newInstance(Optional.of(previousState),
                "G3 X-12.477187 Y14.434376 I28.450704 J-7.79791 F300 ;segmented arc. Max segment length set to 10")));
        assertThat(segmentedStates.get(5), equalTo(previousState = GCodeState.newInstance(Optional.of(previousState),
                "G3 X-10.102253 Y4.76971 I29.424694 J2.106504 F300 ;segmented arc. Max segment length set to 10")));
        assertThat(segmentedStates.get(6), equalTo(previousState = GCodeState.newInstance(Optional.of(previousState),
                "G3 X-4.648695 Y-3.55524 I27.04976 J11.77117 F300 ;segmented arc. Max segment length set to 10")));
        assertThat(segmentedStates.get(7), equalTo(previousState = GCodeState.newInstance(Optional.of(previousState),
                "G3 X3.2628 Y-9.592982 I21.596202 J20.09612 F300 ;segmented arc. Max segment length set to 10")));
        assertThat(segmentedStates.get(8), equalTo(previousState = GCodeState.newInstance(Optional.of(previousState),
                "G3 X12.731797 Y-12.656341 I13.684707 J26.133862 F300 ;segmented arc. Max segment length set to 10")));
        assertThat(segmentedStates.get(9), equalTo(previousState = GCodeState.newInstance(Optional.of(previousState),
                "G3 X22.680598 Y-12.396666 I4.21571 J29.197221 F300 ;segmented arc. Max segment length set to 10")));
        assertThat(segmentedStates.get(10), equalTo(previousState = GCodeState.newInstance(Optional.of(previousState),
                "G3 X31.976897 Y-8.843512 I-5.733091 J28.937546 F300 ;segmented arc. Max segment length set to 10")));
        assertThat(segmentedStates.get(11), equalTo(previousState = GCodeState.newInstance(Optional.of(previousState),
                "G3 X39.56265 Y-2.401274 I-15.02939 J25.384392 F300 ;segmented arc. Max segment length set to 10")));
        assertThat(segmentedStates.get(12), equalTo(GCodeState.newInstance(Optional.of(previousState),
                "G3 X44.5 Y6 I-22.615143 J18.942154 F300")));
    }

    @Test
    public void processState_arcCCWOffsets()
    {
        ArcSegmentProcessor segmentorLength10 = ArcSegmentProcessor.newInstance(Optional.empty(), 10);

        GCodeState firstState = GCodeState.newInstance(Optional.<GCodeState>empty(), "G1X18.5Y46Z-1F3000");
        GCodeState blankState = GCodeState.newInstance(Optional.of(firstState), "");
        GCodeState arcState = GCodeState.newInstance(Optional.of(blankState), "G03 X44.5 Y6 I-1.5 J-29.3 F300");

        segmentorLength10.processState(firstState);
        segmentorLength10.processState(blankState);
        List<GCodeState> segmentedStates = segmentorLength10.processState(arcState);

        GCodeState previousState = blankState;

        assertThat(segmentedStates.get(0), equalTo(previousState = GCodeState.newInstance(Optional.of(previousState),
                "G3 X8.619043 Y44.815824 I-1.5 J-29.3 F300 ;segmented arc. Max segment length set to 10")));
        assertThat(segmentedStates.get(1), equalTo(previousState = GCodeState.newInstance(Optional.of(previousState),
                "G3 X-0.297612 Y40.396679 I8.380957 J-28.115824 F300 ;segmented arc. Max segment length set to 10")));
        assertThat(segmentedStates.get(2), equalTo(previousState = GCodeState.newInstance(Optional.of(previousState),
                "G3 X-7.224027 Y33.251026 I17.297612 J-23.696679 F300 ;segmented arc. Max segment length set to 10")));
        assertThat(segmentedStates.get(3), equalTo(previousState = GCodeState.newInstance(Optional.of(previousState),
                "G3 X-11.363258 Y24.201035 I24.224027 J-16.551026 F300 ;segmented arc. Max segment length set to 10")));
        assertThat(segmentedStates.get(4), equalTo(previousState = GCodeState.newInstance(Optional.of(previousState),
                "G3 X-12.239051 Y14.287985 I28.363258 J-7.501035 F300 ;segmented arc. Max segment length set to 10")));
        assertThat(segmentedStates.get(5), equalTo(previousState = GCodeState.newInstance(Optional.of(previousState),
                "G3 X-9.750638 Y4.652458 I29.239051 J2.412015 F300 ;segmented arc. Max segment length set to 10")));
        assertThat(segmentedStates.get(6), equalTo(previousState = GCodeState.newInstance(Optional.of(previousState),
                "G3 X-4.184333 Y-3.596895 I26.750638 J12.047542 F300 ;segmented arc. Max segment length set to 10")));
        assertThat(segmentedStates.get(7), equalTo(previousState = GCodeState.newInstance(Optional.of(previousState),
                "G3 X3.819413 Y-9.510915 I21.184333 J20.296895 F300 ;segmented arc. Max segment length set to 10")));
        assertThat(segmentedStates.get(8), equalTo(previousState = GCodeState.newInstance(Optional.of(previousState),
                "G3 X13.3397 Y-12.409142 I13.180587 J26.210915 F300 ;segmented arc. Max segment length set to 10")));
        assertThat(segmentedStates.get(9), equalTo(previousState = GCodeState.newInstance(Optional.of(previousState),
                "G3 X23.281136 Y-11.95811 I3.6603 J29.109142 F300 ;segmented arc. Max segment length set to 10")));
        assertThat(segmentedStates.get(10), equalTo(previousState = GCodeState.newInstance(Optional.of(previousState),
                "G3 X32.499873 Y-8.209714 I-6.281136 J28.65811 F300 ;segmented arc. Max segment length set to 10")));
        assertThat(segmentedStates.get(11), equalTo(previousState = GCodeState.newInstance(Optional.of(previousState),
                "G3 X39.935215 Y-1.59524 I-15.499873 J24.909714 F300 ;segmented arc. Max segment length set to 10")));
        assertThat(segmentedStates.get(12), equalTo(GCodeState.newInstance(Optional.of(previousState),
                "G3 X44.5 Y6 I-22.935215 J18.29524 F300")));

    }

    @Test
    public void processState_isArcButIsError()
    {
        ArcSegmentProcessor segmentorLength5 = ArcSegmentProcessor.newInstance(Optional.empty(), 5);

        GCodeState firstState = GCodeState.newInstance(Optional.<GCodeState>empty(), "G0 X4.559 Y24.6414");
        GCodeState secondState = GCodeState.newInstance(Optional.of(firstState), "G1 F6.0 Z0.0");
        GCodeState thirdState = GCodeState.newInstance(Optional.of(secondState), "G3 F10.0 X4.4822 Y24.553 I10.817 J-9.4821");

        segmentorLength5.processState(firstState);
        segmentorLength5.processState(secondState);
        List<GCodeState> segmentedStates = segmentorLength5.processState(thirdState);

        assertThat(segmentedStates.get(0), equalTo(GCodeState.newInstance(Optional.of(secondState),
                "G3 F10.0 X4.4822 Y24.553 I10.817 J-9.4821")));
    }

    @Test
    public void processState_arcCWOffsets()
    {
        ArcSegmentProcessor segmentorLength5 = ArcSegmentProcessor.newInstance(Optional.empty(), 5);

        GCodeState firstState = GCodeState.newInstance(Optional.<GCodeState>empty(), "G1X-64Y39Z-1F3000");
        GCodeState blankState = GCodeState.newInstance(Optional.of(firstState), "");
        GCodeState arcState = GCodeState.newInstance(Optional.of(blankState), "G02 X-50 Y15 I-6 J-19 F300");

        segmentorLength5.processState(firstState);
        segmentorLength5.processState(blankState);
        List<GCodeState> segmentedStates = segmentorLength5.processState(arcState);

        GCodeState previousState = blankState;

        assertThat(segmentedStates.get(0), equalTo(previousState = GCodeState.newInstance(Optional.of(previousState),
                "G2 X-59.469898 Y36.914992 I-6 J-19 F300 ;segmented arc. Max segment length set to 5")));
        assertThat(segmentedStates.get(1), equalTo(previousState = GCodeState.newInstance(Optional.of(previousState),
                "G2 X-55.599428 Y33.770386 I-10.530102 J-16.914992 F300 ;segmented arc. Max segment length set to 5")));
        assertThat(segmentedStates.get(2), equalTo(previousState = GCodeState.newInstance(Optional.of(previousState),
                "G2 X-52.631046 Y29.763168 I-14.400572 J-13.770386 F300 ;segmented arc. Max segment length set to 5")));
        assertThat(segmentedStates.get(3), equalTo(previousState = GCodeState.newInstance(Optional.of(previousState),
                "G2 X-50.7507 Y25.144361 I-17.368954 J-9.763168 F300 ;segmented arc. Max segment length set to 5")));
        assertThat(segmentedStates.get(4), equalTo(previousState = GCodeState.newInstance(Optional.of(previousState),
                "G2 X-50.076178 Y20.203298 I-19.2493 J-5.144361 F300 ;segmented arc. Max segment length set to 5")));
        assertThat(segmentedStates.get(5), equalTo(previousState = GCodeState.newInstance(Optional.of(previousState),
                "G2 X-50.649735 Y15.2495 I-19.923822 J-0.203298 F300 ;segmented arc. Max segment length set to 5")));
        assertThat(segmentedStates.get(6), equalTo(GCodeState.newInstance(Optional.of(previousState),
                "G2 X-50 Y15 I-19.350265 J4.7505 F300")));
    }

    @Test
    public void processState_previousStateShouldEqualLastProcessed()
    {
        ArcSegmentProcessor segmentorLength10 = ArcSegmentProcessor.newInstance(Optional.empty(), 10);

        GCodeState firstState = GCodeState.newInstance(Optional.<GCodeState>empty(), "G1X18.5Y46Z-1F3000");
        GCodeState blankState = GCodeState.newInstance(Optional.of(firstState), "");
        GCodeState arcState = GCodeState.newInstance(Optional.of(blankState), "G03 X44.5 Y6 I-1.5 J-29.3 F300");

        segmentorLength10.processState(firstState);
        segmentorLength10.processState(blankState);
        List<GCodeState> segmentedStates = segmentorLength10.processState(arcState);

        GCodeState firstState2 = GCodeState.newInstance(Optional.empty(), "G0 X8.4583 Y5.1423");
        GCodeState middleState = GCodeState.newInstance(Optional.of(firstState2), "G0 Z0.0625");
        GCodeState nonArcState = GCodeState.newInstance(Optional.of(middleState), "G1 F10.0 Z-0.1");

        List<GCodeState> singleState = segmentorLength10.processState(firstState2);
        List<GCodeState> singleState2 = segmentorLength10.processState(middleState);
        List<GCodeState> singleState3 = segmentorLength10.processState(nonArcState);

        assertThat(singleState.get(0), equalTo(GCodeState.newInstance(
                Optional.of(segmentedStates.get(segmentedStates.size()-1)), "G0 X8.4583 Y5.1423")));
        assertThat(singleState2.get(0), equalTo(GCodeState.newInstance(Optional.of(singleState.get(0)), "G0 Z0.0625")));
        assertThat(singleState3.get(0), equalTo(GCodeState.newInstance(Optional.of(singleState2.get(0)), "G1 F10.0 Z-0.1")));
    }

    @Test
    public void nextState_putZandFBack()
    {
        GCodeState firstState = GCodeState.newInstance(Optional.<GCodeState>empty(), "G1 X15.91549 Y0 Z-1 F300");
        GCodeState blankState = GCodeState.newInstance(Optional.of(firstState), "");
        GCodeState arcState = GCodeState.newInstance(Optional.of(blankState), "G2 X15.91549 Y0 Z-1 I-15.91549 F1000");

        Optional<GCodeState> nextState = segmentor.nextState(arcState);

        assertThat(nextState.get(), equalTo(GCodeState.newInstance(Optional.of(blankState),
                "G2 X-0.000007 Y-15.91549 Z-1 I-15.91549 J0 F1000 ;segmented arc. Max segment length set to 25")));
    }

    @Test
    public void processState_noArcToProcess()
    {
        GCodeState firstState = GCodeState.newInstance(Optional.<GCodeState>empty(), "G1 X18.5 Y46 F3000");
        GCodeState blankState = GCodeState.newInstance(Optional.of(firstState), "");
        GCodeState nonArcState = GCodeState.newInstance(Optional.of(blankState), "G4 P0.5");

        segmentor.processState(firstState);
        segmentor.processState(blankState);
        List<GCodeState> singleState = segmentor.processState(nonArcState);

        assertThat(singleState, contains(nonArcState));
    }
}
