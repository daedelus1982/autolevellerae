package com.cncsoftwaretools.autoleveller.reader;

import com.cncsoftwaretools.autoleveller.Pair;
import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class WordTest
{
    @Test
    public void createDwell()
    {
        Word word = Word.createPair('G', BigDecimal.valueOf(4));

        assertThat(word.getWordPair(), equalTo(Pair.createPair('G', BigDecimal.valueOf(4))));
        assertThat(word.getGroup(), equalTo(WordGroup.NOGROUP));
        assertThat(word.getDescription(), equalTo("Dwell"));
    }

    @Test
    public void createDwellLeadingZero()
    {
        @SuppressWarnings("OctalInteger") Word word = Word.createPair('G', BigDecimal.valueOf(04));

        assertThat(word.getWordPair(), equalTo(Pair.createPair('G', BigDecimal.valueOf(4))));
        assertThat(word.getGroup(), equalTo(WordGroup.NOGROUP));
    }

    @Test
    public void createFeed()
    {
        Word word = Word.createPair('F', BigDecimal.valueOf(1000));

        assertThat(word.getGroup(), equalTo(WordGroup.FEEDGROUP));
    }

    @Test
    public void createXOffset()
    {
        Word word = Word.createPair('I', BigDecimal.valueOf(42.6896));

        assertThat(word.getGroup(), equalTo(WordGroup.NOGROUP));
        assertThat(word.getDescription(), equalTo("X offset for arcs and G87 canned cycles"));
    }

    @Test
    public void createYOffset()
    {
        Word word = Word.createPair('J', BigDecimal.valueOf(76.432));

        assertThat(word.getGroup(), equalTo(WordGroup.NOGROUP));
        assertThat(word.getDescription(), equalTo("Y offset for arcs and G87 canned cycles"));
    }

    @Test
    public void createZOffset()
    {
        Word word = Word.createPair('K', BigDecimal.valueOf(42.6896));

        assertThat(word.getGroup(), equalTo(WordGroup.NOGROUP));
        assertThat(word.getDescription(), equalTo("Z offset for arcs and G87 canned cycles"));
    }

    @Test
    public void createRadius()
    {
        Word word = Word.createPair('R', BigDecimal.valueOf(42.6896));

        assertThat(word.getGroup(), equalTo(WordGroup.NOGROUP));
        assertThat(word.getDescription(), equalTo("Arc radius or canned cycle plane"));
    }

    @Test
    public void createRapidLinearMove()
    {
        Word word = Word.createPair('G', BigDecimal.valueOf(0));

        assertThat(word.getGroup(), equalTo(WordGroup.MOTIONGROUP));
        assertThat(word.getDescription(), equalTo("Rapid motion"));
    }

    @Test
    public void createLinearMoveWithFeed()
    {
        @SuppressWarnings("OctalInteger") Word word = Word.createPair('G', BigDecimal.valueOf(01));

        assertThat(word.getGroup(), equalTo(WordGroup.MOTIONGROUP));
        assertThat(word.getDescription(), equalTo("Linear move with Feed"));
    }

    @Test
    public void createArcMoveWithFeed()
    {
        Word word = Word.createPair('G', BigDecimal.valueOf(2));

        assertThat(word.getGroup(), equalTo(WordGroup.MOTIONGROUP));
        assertThat(word.getDescription(), equalTo("Arc move with Feed (CW)"));
    }

    @Test
    public void createCCWArcMoveWithFeed()
    {
        Word word = Word.createPair('G', BigDecimal.valueOf(3));

        assertThat(word.getGroup(), equalTo(WordGroup.MOTIONGROUP));
        assertThat(word.getDescription(), equalTo("Arc move with Feed (CCW)"));
    }

    @Test
    public void createXAxis()
    {
        Word word = Word.createPair('X', BigDecimal.valueOf(30));

        assertThat(word.getGroup(), equalTo(WordGroup.XAXISGROUP));
        assertThat(word.getDescription(), equalTo("X coordinate"));
    }

    @Test
    public void createYAxis()
    {
        Word word = Word.createPair('Y', BigDecimal.valueOf(10));

        assertThat(word.getGroup(), equalTo(WordGroup.YAXISGROUP));
        assertThat(word.getDescription(), equalTo("Y coordinate"));
    }

    @Test
    public void createZAxis()
    {
        Word word = Word.createPair('Z', BigDecimal.valueOf(-700));

        assertThat(word.getGroup(), equalTo(WordGroup.ZAXISGROUP));
        assertThat(word.getDescription(), equalTo("Z coordinate"));
    }

    @Test
    public void createSpeed()
    {
        Word word = Word.createPair('S', BigDecimal.valueOf(20001));

        assertThat(word.getGroup(), equalTo(WordGroup.SPEEDGROUP));
        assertThat(word.getDescription(), equalTo("Spindle-speed"));
    }

    @Test
    public void createMM()
    {
        Word word = Word.createPair('G', BigDecimal.valueOf(21));

        assertThat(word.getGroup(), equalTo(WordGroup.UNITSGROUP));
        assertThat(word.getDescription(), equalTo("Input in Millimeters"));
    }

    @Test
    public void createInches()
    {
        Word word = Word.createPair('G', BigDecimal.valueOf(20));

        assertThat(word.getGroup(), equalTo(WordGroup.UNITSGROUP));
        assertThat(word.getDescription(), equalTo("Input in Inches"));
    }

    @Test
    public void createAbsolute()
    {
        Word word = Word.createPair('G', BigDecimal.valueOf(90));

        assertThat(word.getGroup(), equalTo(WordGroup.DISTANCEGROUP));
        assertThat(word.getDescription(), equalTo("Absolute distance mode"));
    }

    @Test
    public void createIncremental()
    {
        Word word = Word.createPair('G', BigDecimal.valueOf(91));

        assertThat(word.getGroup(), equalTo(WordGroup.DISTANCEGROUP));
        assertThat(word.getDescription(), equalTo("Incremental distance mode"));
    }

    @Test
    public void createCutterCancel()
    {
        Word word = Word.createPair('G', BigDecimal.valueOf(40));

        assertThat(word.getGroup(), equalTo(WordGroup.CUTTERCOMP));
        assertThat(word.getDescription(), equalTo("Cancel cutter compensation"));
    }

    @Test
    public void createCutterLeftComp()
    {
        Word word = Word.createPair('G', BigDecimal.valueOf(41));

        assertThat(word.getGroup(), equalTo(WordGroup.CUTTERCOMP));
        assertThat(word.getDescription(), equalTo("Cutter compensation left side"));
    }

    @Test
    public void createCutterRightComp()
    {
        Word word = Word.createPair('G', BigDecimal.valueOf(42));

        assertThat(word.getGroup(), equalTo(WordGroup.CUTTERCOMP));
        assertThat(word.getDescription(), equalTo("Cutter compensation right side"));
    }

    @Test
    public void createToollengthOffsetCancel()
    {
        Word word = Word.createPair('G', BigDecimal.valueOf(49));

        assertThat(word.getGroup(), equalTo(WordGroup.TOOLLENGTHOFFSET));
        assertThat(word.getDescription(), equalTo("Tool length offset compensation cancel"));
    }

    @Test
    public void createToollengthOffsetFromTable()
    {
        Word word = Word.createPair('G', BigDecimal.valueOf(43));

        assertThat(word.getGroup(), equalTo(WordGroup.TOOLLENGTHOFFSET));
        assertThat(word.getDescription(), equalTo("Tool length offset compensation from tool table"));
    }

    @Test
    public void createToollengthOffsetDynamic()
    {
        Word word = Word.createPair('G', BigDecimal.valueOf(43.1));

        assertThat(word.getGroup(), equalTo(WordGroup.TOOLLENGTHOFFSET));
        assertThat(word.getDescription(), equalTo("Dynamic tool length offset compensation"));
    }

    @Test
    public void createExactStopMode()
    {
        Word word = Word.createPair('G', BigDecimal.valueOf(61));

        assertThat(word.getGroup(), equalTo(WordGroup.PATHCONTMODE));
        assertThat(word.getDescription(), equalTo("Exact path mode"));
    }

    @Test
    public void createSameExactStopMode()
    {
        Word word = Word.createPair('G', BigDecimal.valueOf(61.1));

        assertThat(word.getGroup(), equalTo(WordGroup.PATHCONTMODE));
        assertThat(word.getDescription(), equalTo("Exact path mode"));
    }

    @Test
    public void createPathControlModeWithOptionalTolerance()
    {
        Word word = Word.createPair('G', BigDecimal.valueOf(64));

        assertThat(word.getGroup(), equalTo(WordGroup.PATHCONTMODE));
        assertThat(word.getDescription(), equalTo("Path control mode with optional tolerance"));
    }

    @Test
    public void createToolChange()
    {
        Word word = Word.createPair('M', BigDecimal.valueOf(6));

        assertThat(word.getGroup(), equalTo(WordGroup.NOGROUP));
        assertThat(word.getDescription(), equalTo("Tool change"));
    }

    @Test
    public void createToolNumber()
    {
        @SuppressWarnings("OctalInteger") Word word = Word.createPair('T', BigDecimal.valueOf(05));

        assertThat(word.getGroup(), equalTo(WordGroup.TOOLGROUP));
        assertThat(word.getDescription(), equalTo("Tool selection"));
    }
}
