package com.cncsoftwaretools.autoleveller;

import com.google.common.collect.ImmutableList;
import javafx.geometry.BoundingBox;
import javafx.geometry.Point2D;
import javafx.geometry.Point3D;
import org.junit.Test;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.cncsoftwaretools.autoleveller.matcher.Point2DMatcher.pointRoundedTo3DP;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class RPFTestIT
{
    @Test
    public void highestZ_mach3()
    {
        String filename = "src/test/ALProbeM3big.test";
        RPF rpf = RPF.newInstance(filename).get();

        assertThat(rpf.highestPoint(), equalTo(new Point3D(1.9492, 0, 0.00898)));
    }

    @Test
    public void lowestZ_mach3()
    {
        String filename = "src/test/ALProbeM3big.test";
        RPF rpf = RPF.newInstance(filename).get();

        assertThat(rpf.lowestPoint(), equalTo(new Point3D(3.89841, 1.54980, -0.01173)));
    }

    @Test
    public void averageZ_mach3()
    {
        String filename = "src/test/ALProbeM3big.test";
        RPF rpf = RPF.newInstance(filename).get();

        assertThat(new BigDecimal(rpf.fetchAverageZ()).setScale(4, BigDecimal.ROUND_HALF_UP),
                equalTo(new BigDecimal(0.0049).setScale(4, BigDecimal.ROUND_HALF_UP)));
    }

    @Test
    public void validSupportedFormats()
    {
        assertThat(RPF.isValidFormat("    X -0.000000Y  0.827691Z  0.016047A  0.000000"), is(true)); //TurboCNC line
        assertThat(RPF.isValidFormat("1.16952,0.77490,0.00460 "), is(true)); //Mach3 line
        assertThat(RPF.isValidFormat("0.000000 0.774900 0.000930 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000"), is(true)); //LinuxCNC line
    }

    @Test
    public void validUnsupportedFormats()
    {
        assertThat(RPF.isValidFormat("X:-0.000000, Y:0.827691, Z:0.016047, A:0.000000"), is(true));
        assertThat(RPF.isValidFormat("1.16952 0.77490 0.00460 "), is(true));
        assertThat(RPF.isValidFormat("1, 2.76, 3"), is(true));
    }

    @Test
    public void invalidFormats()
    {
        assertThat(RPF.isValidFormat("    X -0.000000Y  0.827691Z  "), is(false));
        assertThat(RPF.isValidFormat("1.16952"), is(false));
        assertThat(RPF.isValidFormat("one two three"), is(false));
    }

    @Test
    public void createOrderedPointList_Mach3File() throws IOException
    {
        String filename = "src/test/ALProbeM3big.test";
        List<Point3D> points = RPF.createOrderedPointList(filename);

        assertThat(points.size(), equalTo(99));
        assertThat(points.get(37), equalTo(new Point3D(2.33904, 1.16236, 0.00495)));
    }

    @Test
    public void rpfWithProbeSpacingLT3_5ShouldDeriveInches()
    {
        String filename = "src/test/ALProbeM3big.test";
        RPF inchRPF = RPF.newInstance(filename).get();

        assertThat(inchRPF.deriveUnits(), equalTo(Units.INCHES));
    }

    @Test
    public void rpfWithProbeSpacingGT3_5ShouldDeriveMM()
    {
        String filename = "src/test/ALProbeEMCSmall.test";
        RPF inchRPF = RPF.newInstance(filename).get();

        assertThat(inchRPF.deriveUnits(), equalTo(Units.MM));
    }

    @Test
    public void createOrderedPointList_LinuxCNCFile() throws IOException
    {
        String filename = "src/test/ALProbeEMCBig.test";
        List<Point3D> points = RPF.createOrderedPointList(filename);

        assertThat(points.size(), equalTo(99));
        assertThat(points.get(54), equalTo(new Point3D(3.898400, 1.549800, -0.011729)));
    }

    @Test
    public void createOrderedPointList_TurboCNCFile() throws IOException
    {
        String filename = "src/test/ALProbeTurbo.DAT";
        List<Point3D> points = RPF.createOrderedPointList(filename);

        assertThat(points.size(), equalTo(49));
        assertThat(points.get(22), equalTo(new Point3D(-0.751197, 0.827691, 0.007104)));
    }

    @Test
    public void countRowsInList_smallMesh()
    {
        String filename = "src/test/ALProbeEMCSmall.test";
        int rowCount = RPF.countRowsInList(RPF.createOrderedPointList(filename));

        assertThat(rowCount, is(4));
    }

    @Test
    public void countRowsInList_toleranceCheck()
    {
        List<Point3D> inaccuratePoints = ImmutableList.of(
                new Point3D(-0.48120, -0.34039, 0.00000),
                new Point3D(-0.00000, -0.34039, -0.00058),
                new Point3D(0.48120, -0.34043, -0.00121),
                new Point3D(0.48120, 0.34043, -0.00417),
                new Point3D(-0.00304, 0.34039, -0.00458),
                new Point3D(-0.48420, 0.34039, -0.00488));

        int rowCount = RPF.countRowsInList(inaccuratePoints);

        assertThat(rowCount, is(2));
    }

    @Test
    public void getPointAt_Row3Col4()
    {
        String filename = "src/test/ALProbeEMCSmall.test";
        RPF rpf = RPF.newInstance(filename).get();

        assertThat(rpf.getPointAt(2, 3), equalTo(new Point3D(75.25, 53.5, 0.065645)));
    }

    @Test
    public void getPointAt_Row4Col8()
    {
        String filename = "src/test/ALProbeM3big.test";
        RPF rpf = RPF.newInstance(filename).get();

        assertThat(rpf.getPointAt(3, 7), equalTo(new Point3D(2.72888,1.16236,0.00472)));
        assertThat(rpf.totalPoints(), is(99));
    }

    @Test
    public void createExpandedRPF_columnsLTOriginal()
    {
        RPF rpf = RPF.newInstance("src/test/ALProbeM3big.test").get();

        Optional<RPF> expandedRPF = rpf.createExpandedRPF(10);

        assertThat(expandedRPF, equalTo(Optional.empty()));
    }

    @Test
    public void createExpandedRPF_twoHundred()
    {
        RPF rpf = RPF.newInstance("src/test/ALProbeM3big.test").get();

        RPF expandedRPF = rpf.createExpandedRPF(200).get();
        Point3D lastPoint = expandedRPF.getPointAt(159, 199);
        Point2D lastPoint2D = new Point2D(lastPoint.getX(), lastPoint.getY());

        assertThat(expandedRPF.getColCount(), is(200));
        assertThat(expandedRPF.getRowCount(), is(160));
        assertThat(lastPoint2D, pointRoundedTo3DP(new Point2D(3.898, 3.1)));
    }

    @Test
    public void getSurroundingPoints_randomPoint()
    {
        RPF rpf = RPF.newInstance("src/test/ALProbeM3big.test").get();

        List<Point3D> surroudingPoints = rpf.getSurroundingPoints(new Point2D(1.78, 2.657));

        assertThat(surroudingPoints.get(0), equalTo(new Point3D(1.55936, 2.71215, 0.00643)));
        assertThat(surroudingPoints.get(1), equalTo(new Point3D(1.94920, 2.71215, 0.00652)));
        assertThat(surroudingPoints.get(2), equalTo(new Point3D(1.55936, 2.32470, 0.00537)));
        assertThat(surroudingPoints.get(3), equalTo(new Point3D(1.94920, 2.32470, 0.00527)));
    }

    @Test
    public void getSurroundingPoints_pointOnEdge()
    {
        RPF rpf = RPF.newInstance("src/test/ALProbeM3big.test").get();

        List<Point3D> surroudingPoints = rpf.getSurroundingPoints(new Point2D(1.55936, 1.54980));

        assertThat(surroudingPoints.get(0), equalTo(new Point3D(1.16952, 1.93725, 0.00503)));
        assertThat(surroudingPoints.get(1), equalTo(new Point3D(1.55936, 1.93725, 0.00529)));
        assertThat(surroudingPoints.get(2), equalTo(new Point3D(1.16952, 1.54980, 0.00463)));
        assertThat(surroudingPoints.get(3), equalTo(new Point3D(1.55936, 1.54980, 0.00432)));
    }

    @Test
    public void distinctXandY_removeDuplicates()
    {
        List<Point3D> points = ImmutableList.of(
                new Point3D(-2.629215, 0.000000, -0.000099),
                new Point3D(-2.629215, 0.000000, 0.000050),
                new Point3D(-2.253592, 0.000000, 0.000994),
                new Point3D(-1.878018, 0.000000, 0.002136));

        List<Point3D> distiinctPoints = RPF.distinctList(points);

        assertThat(distiinctPoints, equalTo(ImmutableList.of(
                new Point3D(-2.629215, 0.000000, -0.000099),
                new Point3D(-2.253592, 0.000000, 0.000994),
                new Point3D(-1.878018, 0.000000, 0.002136))));

    }

    @Test
    public void distinctXandY_impreciseRows()
    {
        List<Point3D> inaccuratePoints = ImmutableList.of(
                new Point3D(-0.48120, -0.34039, 0.00000),
                new Point3D(-0.00000, -0.34039, -0.00058),
                new Point3D(0.48120, -0.34043, -0.00121),
                new Point3D(0.48120, 0.34043, -0.00417),
                new Point3D(-0.00304, 0.34039, -0.00458),
                new Point3D(-0.48420, 0.34039, -0.00488));

        List<Point3D> distinctPoints = RPF.distinctList(inaccuratePoints);

        assertThat(distinctPoints, equalTo(inaccuratePoints));
    }
}